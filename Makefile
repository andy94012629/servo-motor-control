PORT = 5 # m128_v2
# PORT = 19# m128_v3
# FILENAME = motor_position
FILENAME = test_velocity

DEVICE = atmega128
LINKER = -Wl,-u,vfprintf -Wl,--start-group -Wl,-lm -Wl,-lprintf_flt -Wl,-lscanf_flt -Wl,--end-group -Wl,--gc-sections -mrelax -Wl,-u,vfscanf
CC = avr-gcc -Wall -Os -mmcu=$(DEVICE) $(LINKER)

LIBSRC += $(wildcard lib/*.c)
TESTSRC += $(wildcard test/*.c)

LIBOBJ += $(patsubst %.c,%.o,$(LIBSRC))

TESTHEX += $(patsubst %.c,%.hex,$(TESTSRC))

default: test upload terminal clean

desp: $(LIBOBJ)
test: $(TESTHEX)

%.hex: %.elf
	@avr-objcopy -j .text -j .data -O ihex $< $@
	
%.elf: %.o $(LIBOBJ)
	@$(CC) $< $(LIBOBJ) -o $@
	@avr-size --format=avr --mcu=$(DEVICE) $@

%.o: %.c
	@$(CC) -c $< -o $@

upload:
	asaloader prog -p COM$(PORT) -f test/$(FILENAME).hex

terminal:
	putty -serial COM$(PORT) -sercfg 38400,8,1,N,N

.PHONY: clean
clean:
	@rm test/*.hex
	@rm lib/*.o

