import serial
import time
import csv
import matplotlib
matplotlib.use("tkAgg")
import matplotlib.pyplot as plt
import numpy as np
import sys

# with open("DAQ.csv", "w"):
#     pass

COM_PORT = 'COM5'
BAUD     = 38400
ser      = serial.Serial(COM_PORT, BAUD)
ser.flushInput()

msg = '800'
ser.write(bytes(msg+'\n', encoding = "utf-8"))

x, y = [],[] 
plt.ion()
fig, ax = plt.subplots()
line, = ax.plot(x,y,'r')
ax.set_autoscale_on(True)
ax.set_ylim([0, 1050])
ax.set_title('ADC Position Command = ' + msg)
count = 0
buffer = 0
while True:
    try:
        ser_bytes = ser.readline()
        # print(ser_bytes)
        try:
            
            # with open("DAQ.csv", "a", newline='') as f:
            #     writer = csv.writer(f, delimiter=",")
            #     writer.writerow([decoded_data])
            buffer += 1
            if buffer > 10:
                decoded_data = int(ser_bytes.decode("utf-8"))
                print(decoded_data)
                x.append(count)
                count += 1 
                y.append(decoded_data)
                line.set_xdata(x)
                line.set_ydata(y)
                ax.relim()
                ax.autoscale_view()
                fig.canvas.draw()
                fig.canvas.flush_events()
                buffer = 0
        except KeyboardInterrupt:
            break
        except:
            continue
        
    except KeyboardInterrupt:
        print("Keyboard Interrupt")
        sys.exit()
