#define LOG10_FROM_2_TO_64_PLUS_1  21
#define UINT64_TO_STR(n)  uint64_to_str(n, (char[21]){0})
#include "../lib/mylib.h"


char *uint64_to_str(uint64_t n, char dest[static 21]) {
    dest += 20;
    *dest-- = 0;
    while (n) {
        *dest-- = (n % 10) + '0';
        n /= 10;
    }
    return dest + 1;
}

int main(void) {
    ASA_M128_set();
  	printf("Hello World\n");
  	printf("%s", UINT64_TO_STR(10000000000ull));
  	return 0;
}