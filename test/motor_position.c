#define F_CPU 11059200UL
#define DUTY_SET(A) OCR1A = (uint16_t)(A * 1.38);

#include "../lib/mylib.h"
#include "../lib/pid.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

//=====修正Libary_ISR_delay問題 start=====
#include <avr/interrupt.h>
#undef ISR
#ifdef __cplusplus
#define ISR(vector, ...)            \
extern "C" void vector (void) __attribute__ ((signal,__INTR_ATTRS)) __VA_ARGS__; \
void vector (void)
#else
#define ISR(vector, ...)            \
void vector (void) __attribute__ ((signal,__INTR_ATTRS)) __VA_ARGS__; \
void vector (void)
#endif
//=====修正Libary_ISR_delay問題 end=====

int num=100;
volatile uint32_t ADC_divid = 0;
volatile uint16_t ADCrealtime = 0;

typedef struct
{
    uint16_t *ADCresult;
    volatile int ADCindex;
}ADC_t;

PID_t PIDStr_t;
ADC_t ADCStr_t;

void PWM_set()
{
    TCCR1A |= (0 << WGM10) | (0 << WGM11); 				/* Phase Correct Mode Select [mode:8] */
    TCCR1A |= (1 << COM1A0) | (1 << COM1A1);
    TCCR1B |= (0 << WGM12) | (1 << WGM13); 				/* Phase Correct Mode Select */
    DDRB |= (1 << PB5);
    TCNT1 = 0;
    TIMSK |= (1 << TICIE1);
    /* 1k Hz */
    // TCCR1B |= (1 << CS10 ) | (1 << CS11) | (0 << CS12); /* Prescaler : 64 */
    // ICR1 = 86;

    /* 40k Hz */
    TCCR1B |= (1 << CS10 ) | (0 << CS11) | (0 << CS12); //Prescaler : 1
	ICR1 = 138;
}

void ADC_set()
{
    ADMUX |= (0 << REFS1) | (0 << REFS0); 					/* ADC_REF_AREF */
    ADCSRA |= (0 << ADPS0)| (0 << ADPS1) | (0 << ADPS2);	/* Prescaler : 2 */
    ADCSRA |= (0 << ADFR);  								/* ADC_CONVMODE_TRIGERED */
    ADCSRA |= (1 << ADEN); 									/* ADC Enable. */
    ADCSRA |= (1 << ADIE);  								/* ADC Interrupt Enable. */
    ADCSRA |= (1 << ADSC);  								/* ADC start conversion. */
}

void InitialFunction(){
    ASA_M128_set();
    PWM_set();
    ADC_set();
    DDRD |= (1 << DDD6);
    PORTD |= (1 << PD6);
    ADCStr_t.ADCresult = (uint16_t *)calloc(num,sizeof(uint16_t));
    ADCStr_t.ADCindex = 0;
    PIDStr_t.PID_lesterror.d_lerr=0;
    PIDStr_t.PID_error.i_err = 0;
    //P_gain = 1.28787 1.36868
    PIDStr_t.PID_gain.Kp = 1.5;
    PIDStr_t.PID_gain.Ki = 0.03;
    PIDStr_t.PID_gain.Kd = 0.8;
    PIDStr_t.Cmd = 0;
}

int main(void)
{
    InitialFunction();
    float lambda = 0.2;
    float err=0;
    OCR1A = 69;
    sei();

    //printf("plz scan cmd\n");
    scanf("%f", &(PIDStr_t.Cmd));
    // printf("cmd = %f", PIDStr_t.Cmd);
   

    while (1) {
        ADCStr_t.ADCresult[ADCStr_t.ADCindex]=ADCrealtime;
        ADCStr_t.ADCindex++;
        for (int i =0; i < ADCStr_t.ADCindex;i++)
        {
            err = PIDStr_t.Cmd - (float)ADCStr_t.ADCresult[i];
            PIDStr_t.PID_error.i_err *= lambda;
            PIDStr_t.PID_error.i_err += err;
        }
        if (ADCStr_t.ADCindex == num)
        {
            ADCStr_t.ADCindex = 0;
        }
        //printf("%f\n",PIDStr_t.Cmd-ADCrealtime);
        if((PIDStr_t.Cmd - (float)ADCrealtime)<= 0)
            PIDStr_t.FB = -48 * (PIDStr_t.PID_gain.Kp*(PIDStr_t.Cmd - (float)ADCrealtime) - 10)/ 1003 + 52;
        else
            PIDStr_t.FB = -47 * (PIDStr_t.PID_gain.Kp*(PIDStr_t.Cmd - (float)ADCrealtime) + 10) / 1003 + 47;
         //printf("duty = %f\n",PIDStr_t.FB);
        PIDStr_t.FB = PIDStr_t.FB + PIDStr_t.PID_gain.Ki*PIDStr_t.PID_error.i_err;
       
        printf("%d\n", ADCrealtime);
        DUTY_SET(PIDStr_t.FB);
    }
    return 0;
}

ISR (TIMER1_CAPT_vect)
{
    ADC_divid++;
    if (!(ADC_divid % 2048)){
        ADCSRA |= (1 << ADSC);  /* ADC conversion */
    }
}

ISR (ADC_vect)
{
    uint8_t resultH = 0;
    uint8_t resultL = 0;
    resultL = ADCL;
    resultH = ADCH;
    ADCrealtime  = (resultH << 8) + resultL;
}