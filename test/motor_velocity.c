#define F_CPU 11059200UL
#define DUTY_SET(A) OCR1A = (uint16_t)(A * 1.38);

#include "../lib/mylib.h"
#include "../lib/pid.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

//=====修正Libary_ISR_delay問題 start=====
#include <avr/interrupt.h>
#undef ISR
#ifdef __cplusplus
#define ISR(vector, ...)            \
extern "C" void vector (void) __attribute__ ((signal,__INTR_ATTRS)) __VA_ARGS__; \
void vector (void)
#else
#define ISR(vector, ...)            \
void vector (void) __attribute__ ((signal,__INTR_ATTRS)) __VA_ARGS__; \
void vector (void)
#endif
//=====修正Libary_ISR_delay問題 end=====

int num=100;
volatile uint32_t ADC_divid = 0;
volatile uint16_t ADCrealtime = 0;
volatile uint16_t ADCpretime = 0;
volatile int count = 0;
volatile float nowspeed = 0;

PID_t PIDStr_t;

void PWM_set()
{
    TCCR1A |= (0 << WGM10) | (0 << WGM11); 				/* Phase Correct Mode Select [mode:8] */
    TCCR1A |= (1 << COM1A0) | (1 << COM1A1);
    TCCR1B |= (0 << WGM12) | (1 << WGM13); 				/* Phase Correct Mode Select */
    DDRB |= (1 << PB5);
    TCNT1 = 0;
    TIMSK |= (1 << TICIE1);

    /* 40k Hz */
    TCCR1B |= (1 << CS10 ) | (0 << CS11) | (0 << CS12); //Prescaler : 1
	ICR1 = 138;
}

void ADC_set()
{
    ADMUX |= (0 << REFS1) | (0 << REFS0); 					/* ADC_REF_AREF */
    ADCSRA |= (0 << ADPS0)| (0 << ADPS1) | (0 << ADPS2);	/* Prescaler : 2 */
    ADCSRA |= (0 << ADFR);  								/* ADC_CONVMODE_TRIGERED */
    ADCSRA |= (1 << ADEN); 									/* ADC Enable. */
    ADCSRA |= (1 << ADIE);  								/* ADC Interrupt Enable. */
    ADCSRA |= (1 << ADSC);  								/* ADC start conversion. */
}

void InitialFunction(){
    ASA_M128_set();
    PWM_set();
    ADC_set();
    DDRD |= (1 << DDD6);
    PORTD |= (1 << PD6);
    PIDStr_t.PID_lesterror.d_lerr=0;
    PIDStr_t.PID_error.i_err = 0;
    //P_gain = 1.28787 1.36868
    PIDStr_t.PID_gain.Kp = 0;
    PIDStr_t.PID_gain.Ki = 0.5;
    PIDStr_t.PID_gain.Kd = 0.8;
    PIDStr_t.Cmd = 0;
}

int main(void)
{
    InitialFunction();
    volatile int flag = 0;
    float err=0;
    float originalpwm = 0;
    
    OCR1A = 69;
    sei();

    scanf("%f", &(PIDStr_t.Cmd));
    // printf("cmd = %f", PIDStr_t.Cmd);
     if (PIDStr_t.Cmd > 0)
    {
        PIDStr_t.PID_gain.Kp = -0.08;
    }else
    {
        PIDStr_t.PID_gain.Kp = 0.01;
    }
    
    PIDStr_t.FB = -1.1772*PIDStr_t.Cmd+50.915;
    originalpwm = PIDStr_t.FB;

    while (1) {
        if (ADCrealtime != 1023)
        {
            ADCpretime = ADCrealtime; 
        }
        if (ADCrealtime != ADCpretime && ADCrealtime == 1023)
        {
            nowspeed = 60/(0.0512*count);
            // printf("%d\t%f\n",count, nowspeed);
            printf("%f\n",nowspeed);
            count = 0;
            ADCpretime = ADCrealtime; 
            if (flag<2)
            {
                flag++;
            }
        }
        if (flag==2)
        {
            err =  -1.1772*(PIDStr_t.Cmd - nowspeed)+50.915;
            PIDStr_t.FB = PIDStr_t.PID_gain.Kp*err + originalpwm;
        }
        DUTY_SET(PIDStr_t.FB);

    }
    return 0;
}

ISR (TIMER1_CAPT_vect)
{
    ADC_divid++;
    if (!(ADC_divid % 2048)){
        ADCSRA |= (1 << ADSC);  /* ADC conversion */
    }
}

ISR (ADC_vect)
{
    uint8_t resultH = 0;
    uint8_t resultL = 0;
    resultL = ADCL;
    resultH = ADCH;
    ADCrealtime  = (resultH << 8) + resultL;
    count++;
}