#define F_CPU 11059200UL
#define PI 3.1415926
#define DUTY_SET(A) OCR1A = (uint16_t)(A * 1.38);

#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/mylib.h"
#include "../lib/pid.h"

void PWM_set()
{
    TCCR1A |= (0 << WGM10) | (0 << WGM11); 				    /* Phase Correct Mode Select [mode:8] */
    TCCR1A |= (1 << COM1A0) | (1 << COM1A1);
    TCCR1B |= (0 << WGM12) | (1 << WGM13); 				    /* Phase Correct Mode Select */
    // TCCR1B |= (1 << CS10 ) | (1 << CS11) | (0 << CS12);   /* Prescaler : 64 */
    TCCR1B |= (1 << CS10 ) | (0 << CS11) | (0 << CS12);     /* Prescaler : 1 */
    DDRB |= (1 << PB5);
    // ICR1 = 86;
    ICR1 = 138;
    // ICR1 = 221;
    TCNT1 = 0;
}

int main(void){
    uint8_t duty = 0;
    ASA_M128_set();
    PWM_set();
    DDRD |= (1 << DDD6) | (1 << DDD5);
    PORTD |= (1 << PD6) | (1 << PD5); /* PD5 : AD8210 Vref1     PD6 : H-Bridge mode & nsleep */
    while(1){
        printf("plz input the duty\n");
        scanf("%d", &duty);
        printf("duty cycle = %d\n", duty);
        DUTY_SET(duty);
    }
    return 0;
}
